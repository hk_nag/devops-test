# ローカルで動作確認

## 前提

- Docker と、 Kubernetes がインストールされていること
  - Docker Desktop であれば、 「Kubernetes を有効にする」だけでOK
  - Linux の人は Minikubeで
    - https://kubernetes.io/docs/tasks/tools/install-minikube/

## 使ってみる

- kubectlが動くことを確認

```
$ kubectl version
```

```
$ kubectl cluster-info
```

- 一般的な操作コマンドを使ってみる

```
$ kubectl get node
```

```
$ kubectl get service
```

```
$ kubectl get deployment
```

```
$ kubectl get pod
```

- 直接、K8sにイメージをデプロイして使ってみる

```
$ kubectl run nginx --image=nginx
```

```
$ kubectl get pod
```

```
$ kubectl get replicaset
```

```
$ kubectl get deployment
```

```
$ kubectl get service # serviceは登録されていない → 外部からアクセスできない
```

```
$ kubectl expose deployment nginx --port=8080 --target-port=80 --type=LoadBalancer
```

```
$ kubectl get service # 外部公開される
```

- `kubectl get service` を行うと、EXTERNAL-IPに外部からアクセスできるIPが表示されます
- http://localhost:8080 でアクセスできることを確認してみてください


## 自動復旧を試す

- 試しに Podを直接削除

```
$ kubectl get pod
```

```
$ kubectl delete pod {POD_NAME}
```

```
$ kubectl get pod  # 新しいPodが起動している
```

- deployment で、1つのNginxのコンテナがPodにあることを定義しているので、Podが障害などで落ちても自動で復旧する
  - 通常は、複数個上げておくと1つPodが死んでも、他のPodが頑張っているうちに復旧するので堅牢なサービス構成になる
  - KubernetesのMasterが生きている必要があるので、Kubernetesはマネージドなものを使うことをお勧め(GKEなど)


## Podのログを確認

```
$ kubectl get pod
```

- 上のコマンドで確認したPodのNameでログを確認する

```
$ kubectl logs -f {POD_NAME}
```

- dockerやdocker-compose と同じ用にログを確認できる
  - http://localhost:8080 にアクセスすればアクセスログが流れるので試す


## クリーンアップ

```
$ kubectl delete service nginx
$ kubectl delete deployment nginx
```



## (オプション) 公式のサンプルをデプロイしてみる

- https://github.com/kubernetes/examples/tree/master/guestbook
  - `frontend-service.yaml` の `spec.type` を `LoadBalancer` に変更してください

```
$ git clone git@github.com:kubernetes/examples.git
$ cd example/guestbook
$ kubectl apply -f redis-master-deployment.yaml 
$ kubectl apply -f redis-master-service.yaml 
$ kubectl apply -f redis-slave-deployment.yaml 
$ kubectl apply -f redis-slave-service.yaml 
$ kubectl apply -f frontend-deployment.yaml 
$ kubectl apply -f frontend-service.yaml
$ kubectl get pod
$ kubectl get service
```

- ブラウザでアクセスして動作できることを確認

- スケールを試す

```
$ kubectl scale deployment frontend --replicas=5
$ kubectl get pods
```

- frontend のPodが増えて、5つになっていることを確認

```
$ kubectl scale deployment frontend --replicas=2
$ kubectl get pods
```

- frontend のPodが2つになっていることを確認
- 運用中に負荷を見て、動的にリソースを増減できる



## クリーンアップ

```
$ kubectl delete deployment frontend
$ kubectl delete deployment redis-master
$ kubectl delete deployment redis-slave
$ kubectl delete service -l app=redis
$ kubectl delete service -l app=guestbook
$ kubectl get pod
$ kubectl get deployment
$ kubectl get service
```


**おつかれさまでした。次のセクションに進みます。**
