# 本資料を作成するにあたって参考にしたサイト

- [Kubernetes全般](https://kubernetes.io/)
- [Kubernetesのサンプル](https://github.com/kubernetes/examples)
- [Skaffold全般](https://skaffold.dev/docs/)
- [Spinnakerの導入チュートリアル](https://cloud.google.com/solutions/continuous-delivery-spinnaker-kubernetes-engine?hl=ja)
- [Istioの導入のチュートリアル](https://codelabs.developers.google.com/codelabs/cloud-hello-istio/index.html)
- [チュートリアル](https://codelabs.developers.google.com/)
- [GitLab.comからDockerイメージをGoogleContainerRegistryにPushする](https://qiita.com/proudust/items/d94c60ec69dead927954)
